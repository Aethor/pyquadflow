from __future__ import annotations
from typing import *


class Rectangle:
    def __init__(self, x: float, y: float, width: float, height: float):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def is_colliding(self, other: Rectangle):
        return (
            self.x < other.x + other.width
            and self.x + self.width > other.x
            and self.y < other.y + other.height
            and self.height + self.y > other.y
        )

    def __str__(self, short: bool = False):
        if short:
            return " ".join([str(v) for v in vars(self).values()])
        return str(vars(self))


class Quadtree:
    def __init__(self, region: Rectangle, max_entities_nb: int, max_depth: int):
        self.entities: Set[Rectangle] = set()
        self.children: Optional[List[Quadtree]] = None
        self.region = region
        self.max_entities_nb = max_entities_nb
        self.max_depth = max_depth

    def is_leaf(self):
        return self.children is None

    def get_entities(self) -> Set[Rectangle]:
        if self.is_leaf():
            return self.entities
        entities: Set[Rectangle] = set()
        for child in self.children:
            entities = entities.union(child.get_entities())
        return entities

    def is_rectangle_colliding(self, rectangle: Rectange) -> bool:
        if self.is_leaf():
            for entity in self.entities:
                if not entity is rectangle and entity.is_colliding(rectangle):
                    return True
            return False

        for child in self.children:
            if child.region.is_colliding(rectangle) and child.is_rectangle_colliding(
                rectangle
            ):
                return True
        return False

    def split_(self):
        """Split leaf into 4 subtrees"""
        child_width = self.region.width / 2
        child_height = self.region.height / 2

        possible_positions = [
            (self.region.x, self.region.y),
            (self.region.x, self.region.y + child_height),
            (self.region.x + child_width, self.region.y + child_height),
            (self.region.x + child_width, self.region.y),
        ]
        self.children = [
            Quadtree(
                Rectangle(x, y, child_width, child_height),
                self.max_entities_nb,
                self.max_depth - 1,
            )
            for x, y in possible_positions
        ]

        for entity in self.entities:
            for child in self.children:
                if child.region.is_colliding(entity):
                    child.insert_(entity)
        self.entities = set()

    def insert_(self, rectangle: Rectangle):
        """Insert a rectangle into the quadtree"""

        if self.is_leaf():
            self.entities.add(rectangle)
            if len(self.entities) > self.max_entities_nb and self.max_depth > 0:
                self.split_()
            return

        for child in self.children:
            if child.region.is_colliding(rectangle):
                child.insert_(rectangle)

    def delete_(self, rectangle: Rectangle):
        """Remove a rectangle from the quadtree"""

        if self.is_leaf():
            if rectangle in self.entities:
                self.entities.remove(rectangle)
            return

        for child in self.children:
            if child.region.is_colliding(rectangle):
                child.delete_(rectangle)
        nested_entities_nb = sum([len(child.get_entities()) for child in self.children])
        if nested_entities_nb <= self.max_entities_nb:
            self.entities = self.get_entities()
            self.children = None

    def move_(self, rectangle: Rectangle, position: Tuple[float]):
        self.delete_(rectangle)
        rectangle.x = position[0]
        rectangle.y = position[1]
        self.insert_(rectangle)

    def __str__(
        self, indent: str = "", is_last: bool = True, is_root: bool = True
    ) -> str:
        string = (
            indent
            + (("└──" if is_last else "├──") if not is_root else "")
            + str([c.__repr__() for c in self.entities])
            + "\n"
        )

        if not self.is_leaf():
            if not is_root:
                indent += "   " if is_last else "│  "
            for child in self.children:
                string += child.__str__(
                    indent=indent, is_last=child is self.children[-1], is_root=False
                )
        return string

    def __len__(self):
        return len(self.get_entities())
