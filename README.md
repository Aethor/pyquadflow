# Pyquadflow

A Python Quadtree implementation for educational purposes. See [the article on the blue-cut blog](https://blue-cut.github.io/datastructures/games/2020/08/02/introduction-to-quadtrees-for-games.html) !
