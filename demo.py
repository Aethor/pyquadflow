from typing import *
import random

from quadflow import Rectangle, Quadtree

import cairo
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk


TICK_FREQUENCY = 60
SPEED_NORM = 500


class Demo:
    def __init__(self):
        self.builder = Gtk.Builder.new_from_file("./demo.glade")

        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", Gtk.main_quit)

        self.load_from_builder("canvas")
        self.canvas.connect("draw", self.on_draw)
        self.canvas.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        self.canvas.connect("button-press-event", self.on_canvas_click)

        self.load_from_builder("bruteforce_computations_nb_label")
        self.load_from_builder("quadtree_computations_nb_label")
        self.load_from_builder("entities_nb_label")

        def draw_tick():
            self.canvas.queue_draw()
            return True

        GLib.timeout_add(TICK_FREQUENCY, draw_tick)

        self.window.show_all()

        self.quadtree = Quadtree(
            Rectangle(
                0,
                0,
                self.canvas.get_allocated_width(),
                self.canvas.get_allocated_height(),
            ),
            3,
            4,
        )

        self.quadtree_collisions_computations_counter: int = 0

        Rectangle.old_is_colliding = Rectangle.is_colliding

        def is_colliding(rectangle_self, other):
            self.quadtree_collisions_computations_counter += 1
            return rectangle_self.old_is_colliding(other)

        Rectangle.is_colliding = is_colliding

    def load_from_builder(self, name: str):
        setattr(self, name, self.builder.get_object(name))

    def draw_quadtree(self, quadtree: Quadtree, cairo_context):
        cairo_context.set_source_rgb(0, 255, 0)
        cairo_context.rectangle(
            quadtree.region.x,
            quadtree.region.y,
            quadtree.region.width,
            quadtree.region.height,
        )
        cairo_context.stroke()
        if not quadtree.is_leaf():
            for q in quadtree.children:
                self.draw_quadtree(q, cairo_context)

    def on_draw(self, canvas, cairo_context):
        self.draw_quadtree(self.quadtree, cairo_context)

        rectangles = self.quadtree.get_entities()
        for rectangle in rectangles:
            if (
                rectangle.x <= 0
                or rectangle.x + rectangle.width >= self.canvas.get_allocated_width()
            ):
                rectangle.speed[0] = -rectangle.speed[0]
            if (
                rectangle.y <= 0
                or rectangle.y + rectangle.height >= self.canvas.get_allocated_height()
            ):
                rectangle.speed[1] = -rectangle.speed[1]
            self.quadtree.move_(
                rectangle,
                (
                    rectangle.x + rectangle.speed[0] * SPEED_NORM / TICK_FREQUENCY,
                    rectangle.y + rectangle.speed[1] * SPEED_NORM / TICK_FREQUENCY,
                ),
            )

        self.quadtree_collisions_computations_counter = 0
        cairo_context.set_source_rgb(0, 0, 0)
        for rectangle in rectangles:
            if self.quadtree.is_rectangle_colliding(rectangle):
                cairo_context.set_source_rgb(255, 0, 0)
            else:
                cairo_context.set_source_rgb(0, 0, 0)
            cairo_context.rectangle(
                rectangle.x, rectangle.y, rectangle.width, rectangle.width
            )
            cairo_context.fill()

        self.quadtree_computations_nb_label.set_text(
            str(self.quadtree_collisions_computations_counter)
        )
        self.bruteforce_computations_nb_label.set_text(
            str(int(len(rectangles) * (len(rectangles) - 1) / 2))
        )
        self.entities_nb_label.set_text(str(len(rectangles)))

    def on_canvas_click(self, canvas, event):
        if event.button == 1:  # left click
            rectangle = Rectangle(event.x, event.y, 20, 20)
            rectangle.speed = [random.random() - 0.5, random.random() - 0.5]
            self.quadtree.insert_(rectangle)


demo_window = Demo()
Gtk.main()
